package org.example;

public class NokiaPhone extends Phone implements PhoneConnection, PhoneMedia{


    public NokiaPhone(String model) {
        super(model);
    }

    @Override
    public void call() {
        System.out.println("Calling...");
    }

    @Override
    public void takecall() {
        System.out.println("take the call...");
    }

    @Override
    public void rejecktcall() {
        System.out.println("to reject call...");
    }

    @Override
    public void photo() {
        System.out.println("make a photo");
    }

    @Override
    public void filming() {
        System.out.println("start to filming..");
    }

    @Override
    public void enlargeoverview() {

    }
}
