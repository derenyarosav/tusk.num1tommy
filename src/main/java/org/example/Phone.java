package org.example;

public abstract class Phone {
private int memory;
private String model;
private int power;
private int year;
public String toString(){
    return model;
}
    public int getMemory() {
        return memory;
    }

    public int getPower() {
        return power;
    }

    public int getYear() {
        return year;
    }

    public String getModel() {
        return model;
    }
    public Phone(String model){
        this.model = model ;
    }
}
